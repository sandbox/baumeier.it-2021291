<?php
/**
 * @file
 * erpal_worksteps.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function erpal_worksteps_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function erpal_worksteps_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function erpal_worksteps_default_entityform_type() {
  $items = array();
  $items['testform'] = entity_import('entityform_type', '{
    "type" : "testform",
    "label" : "testform",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "full_html" },
      "submission_rules" : [],
      "validation_rules" : [],
      "submit_button_text" : "",
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "full_html" },
      "submission_show_submitted" : 0,
      "submissions_view" : "entityforms",
      "user_submissions_view" : "user_entityforms",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : {
        "4" : "4",
        "5" : "5",
        "7" : "7",
        "8" : "8",
        "9" : "9",
        "1" : 0,
        "2" : 0,
        "3" : 0,
        "6" : 0
      },
      "resubmit_action" : "new",
      "access_rules" : [],
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003Efirst testform\\u003C\\/p\\u003E\\r\\n",
        "format" : "full_html"
      }
    },
    "weight" : "0",
    "paths" : []
  }');
  return $items;
}

/**
 * Implements hook_ds_layout_settings_info_alter().
 */
function erpal_worksteps_ds_layout_settings_info_alter(&$data) {
  if (isset($data['node|erpal_task|form'])) {
    $data['node|erpal_task|form']->settings['fields']['field_entityform'] = 'left'; /* WAS: '' */
  }
  if (isset($data['node|erpal_task|full'])) {
    $data['node|erpal_task|full']->settings['fields']['field_entityform'] = 'header'; /* WAS: '' */
  }
}

/**
 * Implements hook_default_page_manager_handlers_alter().
 */
function erpal_worksteps_default_page_manager_handlers_alter(&$data) {
  if (isset($data['node_view_panel_context_2'])) {
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['args'] = ''; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['context'] = array(
      0 => 'argument_entity_id:node_1.nid',
    ); /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['display'] = 'autofields_table'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['feed_icons'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['link_to_view'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['more_link'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['nodes_per_page'] = 50; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['offset'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['override_pager_settings'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['override_title'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['override_title_text'] = ''; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['pager_id'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['panel_args'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['url'] = ''; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['use_pager'] = 1; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->panel = 'sidebar_second_left'; /* WAS: 'sidebar_second_right' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->position = 2; /* WAS: 0 */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->style['style'] = 'default'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->subtype = 'entityforms_current_node'; /* WAS: 'custom_node_content' */
    $data['node_view_panel_context_2']->conf['display']->content['new-16']->type = 'views'; /* WAS: 'custom_node_content' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['context'] = 'argument_entity_id:node_1'; /* WAS: array(
      0 => 'argument_entity_id:node_1',
    ) */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['delta_limit'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['delta_offset'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['delta_reversed'] = 0; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['formatter'] = 'entityreference_entity_view'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['formatter_settings'] = array(
      'view_mode' => 'full',
      'links' => 1,
    ); /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['label'] = 'title'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['override_title'] = 1; /* WAS: 0 */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->configuration['override_title_text'] = 'fillout worksteps'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->panel = 'sidebar_second_left'; /* WAS: 'sidebar_second_right' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->position = 3; /* WAS: 1 */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->style['settings']['pane_collapsed'] = 1; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->style['style'] = 'collapsible'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->subtype = 'node:field_entityform'; /* WAS: 'task_right_sidebar_pane' */
    $data['node_view_panel_context_2']->conf['display']->content['new-17']->type = 'entity_field'; /* WAS: 'panels_mini' */
    $data['node_view_panel_context_2']->conf['display']->content['new-18'] = (object) array(
          'pid' => 'new-18',
          'panel' => 'sidebar_second_right',
          'type' => 'custom_node_content',
          'subtype' => 'custom_node_content',
          'shown' => TRUE,
          'access' => array(
            'plugins' => array(
              0 => array(
                'name' => 'php',
                'settings' => array(
                  'description' => 'Hide on quicktab active',
                  'php' => 'if (function_exists(\'_erpal_projects_helper_hide_task_right_panes\')) {
                    return !_erpal_projects_helper_hide_task_right_panes();
                  }
                  return TRUE;',
                ),
                'not' => FALSE,
              ),
            ),
          ),
          'configuration' => array(
            'types' => array(),
            'field_name' => '',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 0,
          'locks' => array(),
        ); /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->content['new-19'] = (object) array(
          'pid' => 'new-19',
          'panel' => 'sidebar_second_right',
          'type' => 'panels_mini',
          'subtype' => 'task_right_sidebar_pane',
          'shown' => TRUE,
          'access' => array(
            'plugins' => array(
              0 => array(
                'name' => 'php',
                'settings' => array(
                  'description' => 'Hide on quicktab active',
                  'php' => 'if (function_exists(\'_erpal_projects_helper_hide_task_right_panes\')) {
                    return !_erpal_projects_helper_hide_task_right_panes();
                  }
                  return TRUE;',
                ),
                'not' => FALSE,
              ),
            ),
          ),
          'configuration' => array(
            'context' => array(
              0 => 'argument_entity_id:node_1',
            ),
            'override_title' => 0,
            'override_title_text' => '',
          ),
          'cache' => array(),
          'style' => array(
            'settings' => NULL,
          ),
          'css' => array(),
          'extras' => array(),
          'position' => 1,
          'locks' => array(),
        ); /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->panels['sidebar_second_left']['2'] = 'new-16'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->panels['sidebar_second_left']['3'] = 'new-17'; /* WAS: '' */
    $data['node_view_panel_context_2']->conf['display']->panels['sidebar_second_right']['0'] = 'new-18'; /* WAS: 'new-16' */
    $data['node_view_panel_context_2']->conf['display']->panels['sidebar_second_right']['1'] = 'new-19'; /* WAS: 'new-17' */
    unset($data['node_view_panel_context_2']->conf['display']->content['new-16']->access['plugins']);
    unset($data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['field_name']);
    unset($data['node_view_panel_context_2']->conf['display']->content['new-16']->configuration['types']);
    unset($data['node_view_panel_context_2']->conf['display']->content['new-17']->access['plugins']);
  }
}
