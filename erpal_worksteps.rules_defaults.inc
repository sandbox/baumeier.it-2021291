<?php
/**
 * @file
 * erpal_worksteps.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function erpal_worksteps_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_entityform_node'] = entity_import('rules_config', '{ "rules_redirect_entityform_node" : {
      "LABEL" : "redirect entityform node",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "entityform" ],
      "ON" : [ "entityform_insert", "entityform_update" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "entityform" ], "field" : "field_task_ref" } },
        { "NOT data_is_empty" : { "data" : [ "entityform:field-task-ref" ] } }
      ],
      "DO" : [ { "redirect" : { "url" : [ "entityform:field-task-ref:url" ] } } ]
    }
  }');
  return $items;
}
