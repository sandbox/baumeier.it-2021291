<?php
/**
 * @file
 * erpal_worksteps.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function erpal_worksteps_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: ds_layout_settings
  $overrides["ds_layout_settings.node|erpal_task|form.settings|fields|field_entityform"] = 'left';
  $overrides["ds_layout_settings.node|erpal_task|full.settings|fields|field_entityform"] = 'header';

  // Exported overrides for: page_manager_handlers
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|access|plugins"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|args"] = '';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|context"] = array(
    0 => 'argument_entity_id:node_1.nid',
  );
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|display"] = 'autofields_table';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|feed_icons"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|field_name"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|link_to_view"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|more_link"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|nodes_per_page"] = 50;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|offset"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|override_pager_settings"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|override_title"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|override_title_text"] = '';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|pager_id"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|panel_args"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|types"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|url"] = '';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|configuration|use_pager"] = 1;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|panel"] = 'sidebar_second_left';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|position"] = 2;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|style|style"] = 'default';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|subtype"] = 'entityforms_current_node';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-16|type"] = 'views';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|access|plugins"]["DELETED"] = TRUE;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|context"] = 'argument_entity_id:node_1';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|delta_limit"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|delta_offset"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|delta_reversed"] = 0;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|formatter"] = 'entityreference_entity_view';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|formatter_settings"] = array(
    'view_mode' => 'full',
    'links' => 1,
  );
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|label"] = 'title';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|override_title"] = 1;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|configuration|override_title_text"] = 'fillout worksteps';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|panel"] = 'sidebar_second_left';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|position"] = 3;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|style|settings|pane_collapsed"] = 1;
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|style|style"] = 'collapsible';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|subtype"] = 'node:field_entityform';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-17|type"] = 'entity_field';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-18"] = (object) array(
      'pid' => 'new-18',
      'panel' => 'sidebar_second_right',
      'type' => 'custom_node_content',
      'subtype' => 'custom_node_content',
      'shown' => TRUE,
      'access' => array(
        'plugins' => array(
          0 => array(
            'name' => 'php',
            'settings' => array(
              'description' => 'Hide on quicktab active',
              'php' => 'if (function_exists(\'_erpal_projects_helper_hide_task_right_panes\')) {
                return !_erpal_projects_helper_hide_task_right_panes();
              }
              return TRUE;',
            ),
            'not' => FALSE,
          ),
        ),
      ),
      'configuration' => array(
        'types' => array(),
        'field_name' => '',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 0,
      'locks' => array(),
    );
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|content|new-19"] = (object) array(
      'pid' => 'new-19',
      'panel' => 'sidebar_second_right',
      'type' => 'panels_mini',
      'subtype' => 'task_right_sidebar_pane',
      'shown' => TRUE,
      'access' => array(
        'plugins' => array(
          0 => array(
            'name' => 'php',
            'settings' => array(
              'description' => 'Hide on quicktab active',
              'php' => 'if (function_exists(\'_erpal_projects_helper_hide_task_right_panes\')) {
                return !_erpal_projects_helper_hide_task_right_panes();
              }
              return TRUE;',
            ),
            'not' => FALSE,
          ),
        ),
      ),
      'configuration' => array(
        'context' => array(
          0 => 'argument_entity_id:node_1',
        ),
        'override_title' => 0,
        'override_title_text' => '',
      ),
      'cache' => array(),
      'style' => array(
        'settings' => NULL,
      ),
      'css' => array(),
      'extras' => array(),
      'position' => 1,
      'locks' => array(),
    );
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|panels|sidebar_second_left|2"] = 'new-16';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|panels|sidebar_second_left|3"] = 'new-17';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|panels|sidebar_second_right|0"] = 'new-18';
  $overrides["page_manager_handlers.node_view_panel_context_2.conf|display|panels|sidebar_second_right|1"] = 'new-19';

 return $overrides;
}
